<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8"/>
	<title>Document</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<link rel="stylesheet" href="css/custom.css">
	<link rel="stylesheet" href="css/css/flag-icon.min.css">
	<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.11/themes/ui-lightness/jquery-ui.css" type="text/css" media="all" />
	<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
	<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	<script src="https://www.google.com/jsapi" type="text/javascript"></script>
	<script src="js/sly.js"></script>
	<script src="js/custom.js"></script>
</head>

<body>

	<div class="container">
	<header>

		<!-- Start navbar -->
		<nav class="navbar navbar-inverse">
			<div class="container-fluid">

				<div class="navbar-header">
					<!-- Sandwitch button -->
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<!-- End Sandwitch button -->
					<a class="navbar-brand" href="#"><img src="" class="img-responsive" alt="WebSiteLogo"></a>

					<!-- Example 1 -->
					<!-- <form class="navbar-form" role="search">
						<div class="input-group add-on ">
							<input type="text" class="form-control" placeholder="Search" name="srch-term" id="srch-term" />
							<div class="input-group-btn">
								<button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
							</div>
						</div>
					</form> -->
					<!-- Example 2 -->
					<form class="navbar-form navbar-left float-left hidden-xs" role="search">
						<div class="form-group">
							<input type="text" class="form-control search" placeholder="Search">
							<button type="submit" class="btn btn-default search-btn ">Submit</button>
						</div>
					</form>

					<!-- Serach button on mobile -->
					<button type="button" data-toggle="modal" data-target="#mySearch" class="small-btn-search hidden-sm hidden-lg hidden-md btn btn-default">
						<span class="glyphicon glyphicon-search"></span>
					</button>
				</div>
				

				<!-- Area collapsed into sandwitch button -->
				<div class="collapse navbar-collapse" id="myNavbar">

					<ul class="nav navbar-nav navbar-right">
					<!-- Extra Links -->
						<li class="active"><a href="#">Home</a></li>
						<li><a href="#">Page 1</a></li>
						<li><a href="#">Page 2</a></li>
						<!-- <li><a href="#">Page 3</a></li>  -->
						<li></li>
					</ul>
				</div>

			</div>
		</nav>
		<!-- End NavBar -->
		
		<div class="divider"></div>

		<div class="row">
			<!-- Flags -->
			<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
				<div id="flags" class="flags pagespan">
					<div class="scrollbar">
						<div class="handle" style="transform: translateZ(0px) translateX(252px); width: 220px;">
							<div class="mousearea"></div>
						</div>
					</div>
					
					<!-- If you want 'move' | 'by page' method - use ('forward' | 'backward') || ('prevPage' | 'nextPage') -->
					<button class="prev"><i class="glyphicon glyphicon-menu-left"></i></button>
					<button class="next"><i class="glyphicon glyphicon-menu-right"></i></button>

					<div class="frame" style="overflow: hidden;">
						<ul style="transform: translateZ(0px) translateX(-962px); width: 4011px;" class="">
							<li class=""><span class="flag-icon flag-icon-gr flag-icon-squared"></span></li>
							<li class=""><span class="flag-icon flag-icon-ru flag-icon-squared"></span></li>
							<li class=""><span class="flag-icon flag-icon-gb flag-icon-squared"></span></li>
							<li class=""><span class="flag-icon flag-icon-il flag-icon-squared"></span></li>
							<li class=""><span class="flag-icon flag-icon-us flag-icon-squared"></span></li>
							<li class=""><span class="flag-icon flag-icon-br flag-icon-squared"></span></li>
							<li class=""><span class="flag-icon flag-icon-ge flag-icon-squared"></span></li>
						</ul>
					</div>
				</div>
			</div>
			<!-- End Flags -->

			<div class="col-lg-1 col-sm-1 col-md-1 hidden-xs"> <!-- Divider --> </div>
			
			<!-- Categories -->
			<div class="col-lg-7 col-sm-7 col-md-7 col-xs-12">
				<div id="categories" class="categories pagespan">
					<div class="scrollbar">
						<div class="handle" style="transform: translateZ(0px) translateX(252px); width: 220px;">
							<div class="mousearea"></div>
						</div>
					</div>

					<!-- If you want 'move' | 'by page' method - use ('forward' | 'backward') || ('prevPage' | 'nextPage') -->
					<button class="prev"><i class="glyphicon glyphicon-menu-left"></i></button>
					<button class="next"><i class="glyphicon glyphicon-menu-right"></i></button>

					<div class="frame" style="overflow: hidden;">
						<ul style="transform: translateZ(0px) translateX(-962px); width: 4011px;" class="">
							<li class="">Lorem ipsum dolor.</li>
							<li class="">Lorem ipsum dolor.</li>
							<li class="">Lorem ipsum dolor.</li>
							<li class="">Lorem ipsum dolor.</li>
							<li class="">Lorem ipsum dolor.</li>
							<li class="">Lorem ipsum dolor.</li>
							<li class="">Lorem ipsum dolor.</li>
						</ul>
					</div>
				</div>
			</div>

		</div>
	
	</header>

	<div class="divider"></div>

	<section>
		
		<!-- Panel -->
		<div class="panel panel-default">
			<div class="panel-body">You're @<span id="flagtitle"></span> | <span id="cattitle2"></span></div>
		</div>

		<div class="divider"></div>

		<!-- Content -->
		<div class="row">
			<div class="col-sm-7">
				<div class="tab-content" id="contenticons">
					<div id="cat1" class="tab-pane fade in active">
						<a href="#" title="link"><img src="http://placehold.it/90x70" alt="img1" class="img-thumbnail"></a>
						<a href="#" title="link"><img src="http://placehold.it/90x70" alt="img2" class="img-thumbnail"></a>
						<a href="#" title="link"><img src="http://placehold.it/90x70" alt="img3" class="img-thumbnail"></a>
						<a href="#" title="link"><img src="http://placehold.it/90x70" alt="img4" class="img-thumbnail"></a>
						<a href="#" title="link"><img src="http://placehold.it/90x70" alt="img5" class="img-thumbnail"></a>
						<a href="#" title="link"><img src="http://placehold.it/90x70" alt="img6" class="img-thumbnail"></a>
						<a href="#" title="link"><img src="http://placehold.it/90x70" alt="img9" class="img-thumbnail"></a>
						<a href="#" title="link"><img src="http://placehold.it/90x70" alt="img10" class="img-thumbnail"></a>
					</div>
					<div id="cat2" class="tab-pane fade">
						<a href="#" title="link"><img src="http://placehold.it/90x70" alt="img1" class="img-thumbnail"></a>
						<a href="#" title="link"><img src="http://placehold.it/90x70" alt="img2" class="img-thumbnail"></a>
						<a href="#" title="link"><img src="http://placehold.it/90x70" alt="img3" class="img-thumbnail"></a>
						<a href="#" title="link"><img src="http://placehold.it/90x70" alt="img4" class="img-thumbnail"></a>
						<a href="#" title="link"><img src="http://placehold.it/90x70" alt="img5" class="img-thumbnail"></a>
						<a href="#" title="link"><img src="http://placehold.it/90x70" alt="img6" class="img-thumbnail"></a>
						<a href="#" title="link"><img src="http://placehold.it/90x70" alt="img7" class="img-thumbnail"></a>
						<a href="#" title="link"><img src="http://placehold.it/90x70" alt="img9" class="img-thumbnail"></a>
						<a href="#" title="link"><img src="http://placehold.it/90x70" alt="img10" class="img-thumbnail"></a>
					</div>
					<div id="cat3" class="tab-pane fade">
						<a href="#" title="link"><img src="http://placehold.it/90x70" alt="img1" class="img-thumbnail"></a>
						<a href="#" title="link"><img src="http://placehold.it/90x70" alt="img2" class="img-thumbnail"></a>
						<a href="#" title="link"><img src="http://placehold.it/90x70" alt="img3" class="img-thumbnail"></a>
						<a href="#" title="link"><img src="http://placehold.it/90x70" alt="img4" class="img-thumbnail"></a>
						<a href="#" title="link"><img src="http://placehold.it/90x70" alt="img5" class="img-thumbnail"></a>
						<a href="#" title="link"><img src="http://placehold.it/90x70" alt="img6" class="img-thumbnail"></a>
						<a href="#" title="link"><img src="http://placehold.it/90x70" alt="img7" class="img-thumbnail"></a>
						<a href="#" title="link"><img src="http://placehold.it/90x70" alt="img8" class="img-thumbnail"></a>
						<a href="#" title="link"><img src="http://placehold.it/90x70" alt="img9" class="img-thumbnail"></a>
						<a href="#" title="link"><img src="http://placehold.it/90x70" alt="img10" class="img-thumbnail"></a>
					</div>
					<div id="cat4" class="tab-pane fade">
						<a href="#" title="link"><img src="http://placehold.it/90x70" alt="img1" class="img-thumbnail"></a>
						<a href="#" title="link"><img src="http://placehold.it/90x70" alt="img2" class="img-thumbnail"></a>
						<a href="#" title="link"><img src="http://placehold.it/90x70" alt="img3" class="img-thumbnail"></a>
						<a href="#" title="link"><img src="http://placehold.it/90x70" alt="img4" class="img-thumbnail"></a>
						<a href="#" title="link"><img src="http://placehold.it/90x70" alt="img10" class="img-thumbnail"></a>
					</div>
				</div>
			</div>
			<div class="col-sm-5">
				<div id="content">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur, mollitia.
					Non dolorem consequuntur reiciendis, in adipisci dolorum excepturi temporibus eligendi.
					Nam necessitatibus obcaecati consequuntur iste modi vel voluptate totam asperiores.
					Et quibusdam suscipit necessitatibus voluptatum quia, repellat? Soluta, veniam blanditiis.</p>
				</div>
			</div>
		</div>

	</section>

	</div>


<!-- Modal -->
<div id="mySearch" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-body">
				<form role="search">
					<div class="form-group">
						<input type="text" class="form-control" placeholder="Search">
						<button type="submit" class="btn btn-primary " style="margin: 4px 43% -19px;">Search</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

</body>
</html>
