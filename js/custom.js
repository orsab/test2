jQuery(function($){
	'use strict';

	// -------------------------------------------------------------
	//   Basic Navigation
	// -------------------------------------------------------------
	// Flags plug-in
	var $flags = $('#flags');
	var $frame = $flags.find('.frame'); //window.frr = $frame;
	var sly_flags = new Sly($frame, {
		horizontal: 1,
		itemNav: 'forceCentered',
		activateMiddle: 1,
		smart: 1,
		activateOn: 'click',
		mouseDragging: 1,
		touchDragging: 1,
		releaseSwing: 1,
		startAt: 1,
		scrollBar: $flags.find('.scrollbar'),
		scrollBy: 1,
		pagesBar: $flags.find('.pages'),
		activatePageOn: 'click',
		speed: 200,
		moveBy: 600,
		elasticBounds: 1,
		dragHandle: 1,
		dynamicHandle: 1,
		clickBar: 1,

		// Buttons
		forward: $flags.find('.forward'),
		backward: $flags.find('.backward'),
		prev: $flags.find('.prev'),
		next: $flags.find('.next'),
		prevPage: $flags.find('.prevPage'),
		nextPage: $flags.find('.nextPage')
	}).init();

	var $categories = $('#categories');
	var $frame = $categories.find('.frame'); //window.frr = $frame;
	var sly_categories = new Sly($frame, {
		horizontal: 1,
		itemNav: 'forceCentered',
		activateMiddle: 1,
		smart: 1,
		activateOn: 'click',
		mouseDragging: 1,
		touchDragging: 1,
		releaseSwing: 1,
		startAt: 1,
		scrollBar: $categories.find('.scrollbar'),
		scrollBy: 1,
		pagesBar: $categories.find('.pages'),
		activatePageOn: 'click',
		speed: 200,
		moveBy: 600,
		elasticBounds: 1,
		dragHandle: 1,
		dynamicHandle: 1,
		clickBar: 1,

		// Buttons
		forward: $categories.find('.forward'),
		backward: $categories.find('.backward'),
		prev: $categories.find('.prev'),
		next: $categories.find('.next'),
		prevPage: $categories.find('.prevPage'),
		nextPage: $categories.find('.nextPage')
	}).init();
});